/**
 * Created by tshauchenka on 13.04.17.
 */

var main = null;
var projectEnv = null;
$(document).ready(function () {

    main = new Main();
    if($("#env").val() == "prod"){
        projectEnv = "app.php";
    } else {
        projectEnv = "app_dev.php";
    }
});

Main = function () {

};

/**
 * Creates notification manager for further usage
 * @type {NotificationManager}
 */
Main.prototype.notificationManager = new NotificationManager();