<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 08.05.17
 * Time: 13:25
 */

namespace CalendarBundle\Component;


class MonthConstant
{
    public static $month = [
        1 => "Январь",
        2 => "Февраль",
        3 => "Март",
        4 => "Апрель",
        5 => "Май",
        6 => "Июнь",
        7 => "Июль",
        8 => "Август",
        9 => "Сентябрь",
        10 => "Октябрь",
        11 => "Ноябрь",
        12 => "Декабрь"
    ];
}


