<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class BudgetStatController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class BudgetStatController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/budget_stat", name="budget_stat")
     */
    public function indexAction(Request $request)
    {
        $date = new \DateTime();
        return $this->render("@App/budget/monthPage.html.twig",[
            "month" => (int)$date->format("m")
        ]);

    }

    /**
     * @param $month
     * @return JsonResponse
     *
     * @Route("/budget_stat/budget/{month}")
     */
    public function getBudgetStatAction($month)
    {
        /** @var EntityManager $em */
        $em = $this->get("doctrine")->getManager();
        $budgetRecords = $em->getRepository("AppBundle:Budget")->findBy(
            [
                "user"  => $this->getUser(),
                "month" => $month
            ]
        );

        $resultArray = [];
        foreach ($budgetRecords as $record){
            $resultArray[] = [
                "label" => $record->getName(),
                "value" => $record->getSum()
            ];
        }

        return new JsonResponse($resultArray);
    }


    /**
     * @param $month
     * @return JsonResponse
     * @throws \Doctrine\ORM\NoResultException
     *
     * @Route("/budget_stat/ratio/{month}")
     */
    public function getRatioStatAction($month)
    {
        /** @var EntityManager $em */
        $em = $this->get("doctrine")->getManager();

        $budgetSum = $em->getRepository("AppBundle:Budget")->getSumByMonth($month, $this->getUser());
        $expSum = $em->getRepository("AppBundle:Expenditure")->getSumByMonth($month, $this->getUser());

        $resultArray = [];

        $resultArray[] = [
            "m" => 1,
            "a" => $budgetSum,
            "b" => $expSum
        ];


        return new JsonResponse($resultArray);
    }


    /**
     * @param $month
     * @return JsonResponse
     *
     * @Route("/budget_stat/exp/{month}")
     */
    public function getExpStatAction($month)
    {
        /** @var EntityManager $em */
        $em = $this->get("doctrine")->getManager();
        $expRecords = $em->getRepository("AppBundle:Expenditure")->findBy(
            [
                "user"  => $this->getUser(),
                "month" => $month
            ]
        );

        $resultArray = [];
        foreach ($expRecords as $record){
            $resultArray[] = [
                "label" => $record->getName(),
                "value" => $record->getSum()
            ];
        }

        return new JsonResponse($resultArray);
    }

    /**
     * @param $month
     * @return JsonResponse
     *
     * @Route("/budget_stat/expRatio/{month}")
     */
    public function getExpRatioStatAction($month)
    {
        /** @var EntityManager $em */
        $em = $this->get("doctrine")->getManager();
        $expRecords = $em->getRepository("AppBundle:Expenditure")->findBy(
            [
                "user"  => $this->getUser(),
                "month" => $month
            ]
        );

        $closed = 0;
        $open   = 0;

        foreach ($expRecords as $record){
            if($record->getClosed()){
                $closed += $record->getSum();
            } else {
                $open += $record->getSum();
            }
        }

        $resultArray = [
            [
                "label" => "Закрытые",
                "value" => $closed,
            ],
            [
                "label" => "Открытые",
                "value" => $open
            ]
        ];

        return new JsonResponse($resultArray);
    }

    /**
     * @param $month
     * @return int|mixed
     * @Route("/budget_stat/balance/{month}")
     */
    public function getBalanceAction($month)
    {
        /** @var EntityManager $em */
        $em = $this->get("doctrine")->getManager();

        $budgetSum = $em->getRepository("AppBundle:Budget")->getSumByMonth($month, $this->getUser());
        $expSum    = $em->getRepository("AppBundle:Expenditure")->getSumByMonth($month, $this->getUser());

        $total = ($budgetSum - $expSum >= 0) ? $budgetSum - $expSum : 0;

        if($budgetSum > 0){
            return new JsonResponse(floor($total * 100 / $budgetSum));
        }

        return new JsonResponse(0);
    }

    /**
     * @param $month
     * @return int|mixed
     * @Route("/budget_stat/balanceValue/{month}")
     */
    public function getBalanceValueAction($month)
    {
        /** @var EntityManager $em */
        $em = $this->get("doctrine")->getManager();

        $budgetSum = $em->getRepository("AppBundle:Budget")->getSumByMonth($month, $this->getUser());
        $expSum    = $em->getRepository("AppBundle:Expenditure")->getSumByMonth($month, $this->getUser());

        $total = ($budgetSum - $expSum >= 0) ? $budgetSum - $expSum : 0;

        return new JsonResponse($total);
    }

}
