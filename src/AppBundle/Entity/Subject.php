<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subject
 *
 * @ORM\Table(name="subject")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubjectRepository")
 */
class Subject
{

    const TYPE_LECTURE  = 0; //Лекция
    const TYPE_PRACTICE = 1; //Практика

    public static $types = [
        self::TYPE_LECTURE  => "Лекция",
        self::TYPE_PRACTICE => "Практика"
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="Sbj_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Sbj_Name", type="string")
     */
    private $name;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Schedule", mappedBy="subject", cascade={"persist", "remove"})
     */
    private $schedule;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Debt", mappedBy="subject", cascade={"persist", "remove"})
     */
    private $debt;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Teacher", inversedBy="id")
     * @ORM\JoinColumn(name="Sbj_Tch_Id", referencedColumnName="tch_id")
     */
    private $teacher;

    /**
     * @var int
     *
     * @ORM\Column(name="Sbj_Type", type="integer", options={"default":0})
     */
    private $type = self::TYPE_LECTURE;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Subject")
     * @ORM\JoinColumn(name="Sbj_Prt_Id", referencedColumnName="Sbj_Id")
     */
    private $parent;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Subject", mappedBy="parent")
     */
    private $children;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="Sbj_Student_Id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->schedule = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Subject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Subject
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add schedule
     *
     * @param \AppBundle\Entity\Schedule $schedule
     *
     * @return Subject
     */
    public function addSchedule(\AppBundle\Entity\Schedule $schedule)
    {
        $this->schedule[] = $schedule;

        return $this;
    }

    /**
     * Remove schedule
     *
     * @param \AppBundle\Entity\Schedule $schedule
     */
    public function removeSchedule(\AppBundle\Entity\Schedule $schedule)
    {
        $this->schedule->removeElement($schedule);
    }

    /**
     * Get schedule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set teacher
     *
     * @param \AppBundle\Entity\Teacher $teacher
     *
     * @return Subject
     */
    public function setTeacher(\AppBundle\Entity\Teacher $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \AppBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        return self::$types[$this->type];
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Subject $parent
     *
     * @return Subject
     */
    public function setParent(\AppBundle\Entity\Subject $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Subject
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set children
     *
     * @param \AppBundle\Entity\Subject $children
     *
     * @return Subject
     */
    public function setChildren(\AppBundle\Entity\Subject $children = null)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return \AppBundle\Entity\Subject
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add debt
     *
     * @param \AppBundle\Entity\Debt $debt
     *
     * @return Subject
     */
    public function addDebt(\AppBundle\Entity\Debt $debt)
    {
        $this->debt[] = $debt;

        return $this;
    }

    /**
     * Remove debt
     *
     * @param \AppBundle\Entity\Debt $debt
     */
    public function removeDebt(\AppBundle\Entity\Debt $debt)
    {
        $this->debt->removeElement($debt);
    }

    /**
     * Get debt
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDebt()
    {
        return $this->debt;
    }

    /**
     * @param $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get student
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }
}
