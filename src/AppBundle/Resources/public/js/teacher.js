/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var teacherId = $("#teacher_id").data("id");
    var manager = new FormManager();
    manager.setContainer($(".teacherFormBox .form-container"));
    if (teacherId) {
        manager.setUrl( projectEnv + "/teacher/load/" + teacherId);
    } else {
        manager.setUrl( projectEnv + "/teacher/add");

    }
    manager.init();

    var tableManager = new TableManager();
    tableManager.setContainer($(".teacherListBox .box-body"));
    if (teacherId) {
        tableManager.setUrl( projectEnv + "/subject/byTeacher/" + teacherId);
    } else {
        tableManager.setUrl( projectEnv + "/subject/list");

    }
    $(".teacherListBox .box-body").off().on("click", ".teacher-remove", function () {

        remove($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();
});


function remove(id, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/teacher/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}