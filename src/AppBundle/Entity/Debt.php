<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Debt
 *
 * @ORM\Table(name="debt")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DebtRepository")
 */
class Debt
{

    const TYPE_LAB       = 0;
    const TYPE_PRACT_RAB = 1;
    const TYPE_ZACHET    = 2;
    const TYPE_EXAM      = 3;
    const TYPE_PRACTICE  = 4;

    //константы для типизации оценок
    const SINGLE = 0;
    const BUNDLE = 1;

    public static $types = [
        self::TYPE_LAB       => "Лабораторная",
        self::TYPE_PRACT_RAB => "Практическая",
        self::TYPE_ZACHET    => "Зачёт",
        self::TYPE_EXAM      => "Экзамен",
        self::TYPE_PRACTICE  => "Практика",
    ];

    public static $valueTypes = [
        self::TYPE_LAB       => self::BUNDLE,
        self::TYPE_PRACT_RAB => self::BUNDLE,
        self::TYPE_ZACHET    => self::SINGLE,
        self::TYPE_EXAM      => self::SINGLE,
        self::TYPE_PRACTICE  => self::SINGLE
    ];


    /**
     * @var int
     *
     * @ORM\Column(name="Dbt_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Subject", inversedBy="id")
     * @ORM\JoinColumn(name="Dbt_Sbj_Id", referencedColumnName="Sbj_Id")
     */
    private $subject;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DebtValue", mappedBy="debt", cascade={"remove"})
     */
    private $values;

    /**
     * @var integer
     * @ORM\Column(name="Obt_Type", type="integer")
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param \AppBundle\Entity\Subject $subject
     *
     * @return Debt
     */
    public function setSubject(\AppBundle\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \AppBundle\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add value
     *
     * @param \AppBundle\Entity\DebtValue $value
     *
     * @return Debt
     */
    public function addValue(\AppBundle\Entity\DebtValue $value)
    {
        $this->values[] = $value;

        return $this;
    }

    /**
     * Remove value
     *
     * @param \AppBundle\Entity\DebtValue $value
     */
    public function removeValue(\AppBundle\Entity\DebtValue $value)
    {
        $this->values->removeElement($value);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Debt
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        return self::$types[$this->type];
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        /** @var DebtValue $value */
        foreach ($this->getValues() as $value){
            if($value->getValue() == 0){
                return false;
            }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getValueType()
    {
        return self::$valueTypes[$this->type];
    }
}
