<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 20.04.17
 * Time: 19:42
 */

namespace CalendarBundle\Component;


use AppBundle\Entity\Schedule;

class ScheduleCalculator
{
    public function getStartDate(Schedule $schedule)
    {
        $calDate = new CalendarDate();
        $date = $calDate->getWeekStart();
        $date->modify("+" . $schedule->getDow() . " days");

        return $date;
    }

}