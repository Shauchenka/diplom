/**
 * Created by tshauchenka on 12.04.17.
 */
var prefix = "appbundle_subject_";
var subjectId;
var manager;
var scheduleManager;
var scheduleListManager;

var debtFormManager;

$(document).ready(function () {
    subjectId = $("#subject_id").data("id");
    manager = new FormManager();
    var container = $(".subjectFormBox .form-container");
    manager.setContainer(container);

    if (subjectId) {
        manager.setUrl(projectEnv + "/subject/view/" + subjectId);
    } else {
        manager.setUrl(projectEnv + "/subject/load");
    }

    manager.onLoad(function () {
        //скрыть поле для выбора преподавателя лаб
        if ($(container).find(":checkbox[value='1']:not(:checked)").length > 0) {
            $("#" + prefix + "teacher_labs").closest(".form-group").hide();
        }
    });

    manager.onSuccessSubmit(function () {
        if (subjectId) {
            //загрузить вид просмотра после добавления/изменения
            manager.setUrl(projectEnv + "/subject/view/" + subjectId);
            manager.init();
        }
    });

    manager.init();

    scheduleManager = new FormManager();
    var scheduleContainer = $(".scheduleBox .form-container");

    scheduleListManager = new FormManager();
    var scheduleListContainer = $(".scheduleListBox .form-container");
    var debtContainer = $(".debtFormBox .form-container");

    var closedDebtListManager = new FormManager();
    var closedDebtListContainer = $(".closedDebtListBox .form-container");

    var debtListManager = new FormManager();
    var debtListContainer = $(".debtListBox .form-container");

    debtFormManager = new FormManager();

    if (subjectId) {
        scheduleManager.setUrl(projectEnv + "/schedule/" + subjectId);
        scheduleManager.setContainer(scheduleContainer);
        scheduleManager.init();

        scheduleManager.onSuccessSubmit(function () {
            scheduleManager.init();
            scheduleListManager.init();
        });
        scheduleListManager.setUrl(projectEnv + "/schedule/list/" + subjectId);
        scheduleListManager.setContainer(scheduleListContainer);
        scheduleListManager.init();
    } else {
        $(scheduleContainer).closest(".box").hide();
        $(scheduleListContainer).closest(".box").hide();
        $(debtListContainer).closest(".box").hide();
        $(closedDebtListContainer).closest(".box").hide();
        $(debtContainer).closest(".box").hide();
    }

    debtFormManager.setUrl(projectEnv + "/debt/form/" + subjectId);
    debtFormManager.setContainer(debtContainer);
    debtFormManager.onRegisterEvents(function () {
        $(debtContainer).on("change", ".choiceDebtType", function () {
            debtFormManager.reload(this.value);
        });
    });

    closedDebtListManager.setUrl(projectEnv + "/debt/list/" + subjectId + "/closed");
    closedDebtListManager.setContainer(closedDebtListContainer);
    closedDebtListManager.onLoad(function () {
        $(closedDebtListContainer).find("input, select").attr("disabled", "disabled");
    });

    debtListManager.setUrl(projectEnv + "/debt/list/" + subjectId + "/hide");
    debtListManager.setContainer(debtListContainer);

    debtListManager.onRegisterEvents(function () {
        $(debtListContainer).on("click", ".btnDebtSave", function () {
            if ($(this).data("type") == 0) {
                var value = 0;
                value += $(this).closest("tr").find(".debtValueCheckbox")[0].checked;
            } else {
                var value = $(this).closest("tr").find(".DebtValueDropdown").find("option:checked").val();
            }
            $.ajax({
                url:  window.location.origin + "/" + projectEnv + "/debt/save/" + $(this).data("id"),
                data: "value=" + value
            })
                .done(function () {
                    debtListManager.init();
                    closedDebtListManager.init();
                })
                .error(function (data) {
                    main.notificationManager.errorNotify(data);
                })

        });
    });

    debtFormManager.onSuccessSubmit(function () {
        debtListManager.init();
    });

    if (subjectId) {
        closedDebtListManager.init();
        debtListManager.init();
        debtFormManager.init();
    }

    registerEvents(container);

});

function registerEvents(container) {
    $(container)
        .on("change", ":checkbox[value='1']", function () {
            if (this.checked) {
                $("#" + prefix + "teacher_labs").closest(".form-group").show();
            } else {
                $("#" + prefix + "teacher_labs").closest(".form-group").hide();
            }
        })
        .on("click", ".edit-subject", function () {
            manager.setUrl(projectEnv + "/subject/load/" + subjectId);
            manager.init();
        })
}
