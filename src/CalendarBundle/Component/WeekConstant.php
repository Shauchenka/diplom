<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 05.04.17
 * Time: 16:31
 */

namespace CalendarBundle\Component;


class WeekConstant
{
    const MONDAY = 0; //Понедельник
    const TUESDAY = 1; //Вторник
    const WEDNESDAY = 2; //Среда
    const THURSDAY = 3; //Четверг
    const FRIDAY = 4; //Пятница
    const SATURDAY = 5; //Суббота
    const SUNDAY = 6; //Воскресенье

    public static $daysOfWeek = [
        self::MONDAY => "Понедельник",
        self::TUESDAY => "Вторник",
        self::WEDNESDAY => "Среда",
        self::THURSDAY => "Четверг",
        self::FRIDAY => "Пятница",
        self::SATURDAY => "Суббота",
        self::SUNDAY => "Воскресенье",

    ];


    public static $startEvent = [
        1 => "08:00",
        2 => "09:45",
        3 => "11:30",
        4 => "13:30",
        5 => "15:10",
        6 => "16:40",
        7 => "18:20",
        8 => "20:00"
    ];

    /**
     * @param $number
     * @return string
     */
    public static function getEndEvent($number)
    {
        $start = self::$startEvent[$number];
        $date = new \DateTime();
        $startArray = explode(":", $start);
        $date->setTime($startArray[0], $startArray[1]);
        $date->modify("+90 minutes");

        return $date->format("H:i");
    }

    const TYPE_ABOVE = 0;
    const TYPE_UNDER = 1;

    public static $weekType = [
        self::TYPE_ABOVE => "Над чертой",
        self::TYPE_UNDER => "Под чертой",
    ];

    /**
     * @return int
     */
    public static function getWeekType(){
        $test = strtotime("03.02.2017"); // тут может преобразование отличаться
        $number = date("W", $test);
        if($number % 2 == 0){
            return WeekConstant::TYPE_ABOVE;
        }

        return WeekConstant::TYPE_UNDER;
    }

}