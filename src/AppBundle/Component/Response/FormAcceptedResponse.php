<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 13.04.17
 * Time: 18:44
 */

namespace AppBundle\Component\Response;


use Symfony\Component\HttpFoundation\Response;

class FormAcceptedResponse extends Response
{
    function __construct($content = "")
    {
        parent::__construct($content, 202);
    }
}
