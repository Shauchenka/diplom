<?php

namespace CalendarBundle\Component;

use AppBundle\Entity\Schedule;
use CalendarBundle\Entity\Event;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 04.04.17
 * Time: 17:53
 */
class CalendarBuilder
{
    /** @var  EntityManager */
    private $em;

    /** @var  TokenStorage */
    private $security;

    /**
     * CalendarBuilder constructor.
     * @param EntityManager $em
     * @param TokenStorage $security
     */
    public function __construct(EntityManager $em, TokenStorage $security)
    {
        $this->em = $em;
        $this->security = $security;
    }

    /**
     * @param \DateTime $date
     * @return array
     */
    public function getData(\DateTime $date)
    {

        $resultArray = [];
        $result = $this->em->getRepository("AppBundle:Schedule")->findBy([
            "user" => $this->security->getToken()->getUser(),
            "weekType" => [WeekConstant::getWeekType(), 2]
        ]);

        //generate array width events grouped by dow
        /** @var Schedule $oneResult */
        foreach ($result as $oneResult) {
            $resultArray[$oneResult->getDow()][] = $oneResult;
        }

        return $resultArray;

    }
    /**
     * @param \DateTime $date
     * @return array
     */
    public function getUserData(\DateTime $date)
    {
        $date = CalendarDate::convert($date);

        $resultArray = [];
        $result = $this->em->getRepository("CalendarBundle:Event")->findBy([
            "user" => $this->security->getToken()->getUser()
        ]);

        //generate array width events grouped by dow
        /** @var Event $oneResult */
        foreach ($result as $oneResult) {
            /** @var \DateTime $newDate */
            $newDate = $oneResult->getStartDate();
            $dow = date("w", $newDate->getTimestamp());
            $dow = ($dow - 1 < 0) ? $dow + 7 : $dow - 1;

            $resultArray[$dow][] = $oneResult;
        }

        return $resultArray;

    }

}