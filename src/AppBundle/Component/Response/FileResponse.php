<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 14.05.2017
 * Time: 22:05
 */

namespace AppBundle\Component\Response;
use Symfony\Component\HttpFoundation\Response;

class FileResponse extends Response
{
    function __construct($filename, $content = "")
    {
        // Generate response
        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', $content);
        $response->headers->set('Content-Disposition', 'attachment; filename="' .($filename) . '";');
        $response->headers->set('Content-length', sizeof($content));

        $response->sendHeaders();

        $response->setContent($content);

       return $response;
    }
}