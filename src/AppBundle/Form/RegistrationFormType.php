<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("first", null, [
                "label" => "Имя"
            ])
            ->add("last", null, [
                "label" => "Фамилия"
            ])
            ->add('profilePictureFile')
            ->add("save", SubmitType::class, [
                "label" => "Регистрация",
                "attr" => [
                    "class" => "btn btn-danger center-block"
                ]
            ]);
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_user_registration';
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'fos_user_registration';
    }

}
