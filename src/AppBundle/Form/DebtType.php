<?php

namespace AppBundle\Form;

use AppBundle\Entity\Debt;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DebtType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', ChoiceType::class, [
            "choices" => Debt::$types,
            "label"   => "Тип",
            "attr" => [
                "class" => "choiceDebtType"
            ]
        ]);

        /** @var Debt $data */
        $data = $builder->getData();


        if (is_null($data->getType()) || (array_key_exists($data->getType(), Debt::$valueTypes) && Debt::$valueTypes[$data->getType()])) {
            $readOnly = false;
            $dataValue = 0;
        } else {
            $readOnly = true;
            $dataValue = 1;
        }

        $builder
            ->add("subjectId", HiddenType::class, [
                "data" => $data->getSubject()->getId(),
                "mapped" => false
            ])
            ->add("count", IntegerType::class, [
            "label"   => "Колличество",
            "read_only" => $readOnly,
            'data' => $dataValue,
            "mapped" => false
        ])
            ->add("save", SubmitType::class, [
                "label" => "Добавить",
                "attr" => [
                    "class" => "btn btn-danger pull-right"
                ]
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Debt'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_debt';
    }


}
