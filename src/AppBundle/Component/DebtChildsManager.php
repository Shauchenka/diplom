<?php

namespace AppBundle\Component;

use AppBundle\Entity\Debt;
use AppBundle\Entity\DebtValue;
use Doctrine\ORM\EntityManager;

/**
 * Created by PhpStorm.
 * User: taras
 * Date: 01.05.2017
 * Time: 12:56
 */
class DebtChildsManager
{
    /** @var  EntityManager */
    private $em;

    /**
     * debtChildsManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Debt $debt
     * @param int $count
     */
    public function createChilds(Debt $debt, $count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $debtValue = new DebtValue();
            $debtValue->setDebt($debt);
            $grade = Debt::$valueTypes[$debt->getType()];
            $debtValue->setGrade($grade);
            $this->em->persist($debtValue);
            $debt->addValue($debtValue);
        }
        $this->em->flush();
    }


}