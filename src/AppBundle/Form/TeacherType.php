<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class TeacherType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first', null, [
                "label" => "Имя",
                'constraints' => new Length(array('min' => 3)),
                "attr" => [
                    "placeholder" => "Введите имя"
                ]
            ])
            ->add('last', null, [
                "label" => "Фамилия",
                "attr" => [
                    "placeholder" => "Введите фамилию"
                ]
            ])
            ->add('patronymic', null, [
                "label" => "Отчество",
                "attr" => [
                    "placeholder" => "Введите отчество"
                ]
            ])
            ->add("save", SubmitType::class, [
                "label" => "Сохранить",
                "attr" => [
                    "class" => "btn btn-danger pull-right"
                ]
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Teacher'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_teacher';
    }


}
