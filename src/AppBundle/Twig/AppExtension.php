<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Schedule;
use AppBundle\Entity\Subject;
use CalendarBundle\Component\CalendarDate;
use CalendarBundle\Component\MonthConstant;
use CalendarBundle\Component\WeekConstant;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 20.04.17
 * Time: 15:59
 */
class AppExtension extends \Twig_Extension
{

    private $container;

    /**
     * AppExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('studyDays', array($this, 'getStudyDays')),
            new \Twig_SimpleFunction('successCountBySubject', array($this, 'getSuccessCountBySubject')),
            new \Twig_SimpleFunction('totalCountBySubject', array($this, 'getTotalCountBySubject')),
            new \Twig_SimpleFunction('randomColor', array($this, 'getRandomColor')),
            new \Twig_SimpleFunction('calcTotalProgress', array($this, 'calcTotalProgress')),
            new \Twig_SimpleFunction('weekInfo', array($this, 'getWeekInfo')),
            new \Twig_SimpleFunction('weekType', array($this, 'getWeekType')),
            new \Twig_SimpleFunction('startEventDate', array($this, 'getStartEventDate')),
            new \Twig_SimpleFunction('endEventDate', array($this, 'getEndEventDate')),
            new \Twig_SimpleFunction('monthName', array($this, 'getMonthName')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('dayName', array($this, 'getDayName')),
        );
    }

    /**
     * @param $month
     * @return mixed
     */
    public function getMonthName($month)
    {
        if(array_key_exists($month, MonthConstant::$month)){
            return MonthConstant::$month[$month];
        }

        return MonthConstant::$month[1];
    }

    /**
     * @return array
     */
    public function getStudyDays(){
        $week = WeekConstant::$daysOfWeek;
        array_pop($week);

        return $week;
    }

    /**
     * @param Subject $subject
     * @return float
     */
    public function getSuccessCountBySubject(Subject $subject)
    {
        $successDebtCount = $this->container->get("doctrine")->getRepository("AppBundle:Subject")->getSuccessDebtValueCount($subject);

        return $successDebtCount;
    }
    /**
     * @param Subject $subject
     * @return float
     */
    public function getTotalCountBySubject(Subject $subject)
    {
        $debtCount = $this->container->get("doctrine")->getRepository("AppBundle:Subject")->getDebtValueCount($subject);

        return $debtCount;
    }

    /**
     * @return mixed
     */
    public function getRandomColor()
    {
        $colors = [
            "bg-blue",
            "bg-yellow",
            "bg-green",
            "bg-purple",
            "bg-red",
        ];

        return $colors[mt_rand(0, count($colors)-1)];
    }

    public function calcTotalProgress()
    {
        $user = $this->container->get("security.token_storage")->getToken()->getUser();
        $success = $this->container->get("doctrine")->getRepository("AppBundle:Subject")->getSuccessDebtValueCountByUser($user);
        $total = $this->container->get("doctrine")->getRepository("AppBundle:Subject")->getDebtValueCountByUser($user);

        if($total == 0) {
            return 100;
        }

        return floor($success * 100 / $total);
    }


    /**
     * @param $number
     * @return null
     */
    public function getDayName($number){
        if(array_key_exists($number, WeekConstant::$daysOfWeek)){
            return WeekConstant::$daysOfWeek[$number];
        }

        return null;
    }

    /**
     * @param \DateTime $date
     * @return string
     */
    public function getWeekInfo(\DateTime $date)
    {
        $date = new  CalendarDate($date->format("Y-m-d"));

        return $date->getWeekStart()->format("Y-m-d")  . "  " . $date->getWeekEnd()->format("Y-m-d");

    }

    /**
     * @param Schedule $schedule
     */
    public function getStartEventDate(Schedule $schedule)
    {

        return WeekConstant::$startEvent[$schedule->getNumber()];
    }

    /**
     * @param Schedule $schedule
     * @return string
     */
    public function getEndEventDate(Schedule $schedule)
    {
        return WeekConstant::getEndEvent($schedule->getNumber());
    }

    /**
     * @return mixed
     */
    public function getWeekType()
    {
        $test = strtotime("03.02.2017"); // тут может преобразование отличаться
        $number = date("W", $test);
        if($number % 2 == 0){
            return WeekConstant::$weekType[WeekConstant::TYPE_ABOVE];
        }

        return WeekConstant::$weekType[WeekConstant::TYPE_UNDER];
    }
}