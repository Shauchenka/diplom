<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 05.05.2017
 * Time: 0:54
 */

namespace AppBundle\Controller;


use AppBundle\Component\Response\FormAcceptedResponse;
use CalendarBundle\Entity\Event;
use CalendarBundle\Form\EventType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class EventController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class EventController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/event", name="event_page")
     * @Route("/event/{id}", name="event_page_with_id", requirements={ "id": "\d+" })
     */
    public function indexAction(Request $request, Event $event = null)
    {
        return $this->render("@App/event/page.html.twig", [
            "id" => is_null($event) ? null : $event->getId()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/event/form", name="form_event")
     */
    public function formAction(Request $request)
    {
        $form = $this->createForm(new EventType(), new Event(), [
            "action" => $this->generateUrl("form_event")
        ]);

        $form->handleRequest($request);
        
        if($form->isValid()){
            /** @var EntityManager $manager */
            $manager = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $data->setUser($this->getUser());
            $data->setStartDate(new \DateTime( $data->getStartDate()));
            $data->setEndDate(new \DateTime( $data->getEndDate()));
            $manager->persist($data);
            $manager->flush();
            
            return new FormAcceptedResponse();
        }
        
        return $this->render("@App/event/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/event/edit/{id}", name="form_event_id")
     */
    public function editAction(Request $request, Event $event)
    {
        $form = $this->createForm(new EventType(), $event, [
            "action" => $this->generateUrl("form_event_id", [
                "id" => $event->getId()
            ])
        ]);

        $form->handleRequest($request);
        
        if($form->isValid()){
            /** @var EntityManager $manager */
            $manager = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $data->setUser($this->getUser());
            $data->setStartDate(new \DateTime( $data->getStartDate()));
            $data->setEndDate(new \DateTime( $data->getEndDate()));

            $manager->flush();
            
            return new FormAcceptedResponse();
        }
        
        return $this->render("@App/event/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/event/listPage", name="event_list_page")
     */
    public function getListPageAction(Request $request)
    {
        return $this->render("@App/event/eventListPage.html.twig");
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/event/list", name="event_list")
     */
    public function getListAction(Request $request)
    {
        $events = $this->getDoctrine()->getRepository("CalendarBundle:Event")->findBy([
            "user" => $this->getUser()
        ]);

        return $this->render("@App/event/list.html.twig", [
           "events" => $events
        ]);
    }

    /**
     * @Route("/event/remove/{id}", name="event_remove")
     * @param Event $event
     * @return FormAcceptedResponse
     */
    public function remove(Event $event)
    {
        $this->getDoctrine()->getManager()->remove($event);
        $this->getDoctrine()->getManager()->flush();

        return new FormAcceptedResponse();
    }

}