/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var eventId = $("#event_id").data("id");

    var manager = new FormManager();
    var container = $(".eventFormBox .form-container");
    manager.setContainer(container);
    manager.onLoad(function () {
         $(container).find(".datepicker").datetimepicker();
    });
    if(eventId){
        manager.setUrl(projectEnv + "/event/edit/" + eventId);
    } else {
        manager.setUrl(projectEnv + "/event/form");
    }
    manager.init();

});