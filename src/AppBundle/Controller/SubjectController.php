<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 18.04.17
 * Time: 10:14
 */

namespace AppBundle\Controller;


use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\Subject;
use AppBundle\Entity\Teacher;
use AppBundle\Form\SubjectType;
use CalendarBundle\Entity\Event;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class SubjectController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class SubjectController extends Controller
{
    /**
     * @Route("/subject", name="subject")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render("@App/subjectListPage.html.twig");
    }

    /**
     *
     * @Route("/subject/list", name="subject_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $subjects = $this->getDoctrine()->getRepository("AppBundle:Subject")->findBy([
            "user" => $this->getUser()
        ]);

        return $this->render("@App/subjectList.html.twig", [
            "subjects" => $subjects
        ]);
    }

    /**
     * @Route("/subject/add", name="subject_form")
     * @Route("/subject/{id}", name="subject_form_id", requirements={ "id": "\d+" })
     */
    public function pageAction(Request $request, Subject $subject = null)
    {
        if (!is_null($subject)) {
            return $this->render("@App/subject.html.twig", [
                "id" => $subject->getId()
            ]);
        }
        return $this->render("@App/subject.html.twig");
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormView
     * @Route("/subject/load", name="subject_load")
     * @Route("/subject/load/{id}", name="subject_load_with_id")
     */
    public function loadAction(Request $request, Subject $subject = null)
    {
        $saveSubject = null;
        $flagNew = false;
        if (is_null($subject)) {
            $subject = new Subject();
            $flagNew = true;
        } else {
            $saveSubject = clone($subject);
        }

        if ($flagNew) {
            $action = $this->generateUrl("subject_load");
        } else {

            $action = $this->generateUrl("subject_load_with_id", [
                "id" => $subject->getId()
            ]);
        }

        $form = $this->createForm(new SubjectType(), $subject, [
            "action" => $action,
            "user" => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            if ($flagNew) {
                $types = $form->get("type")->getData();
                $em->persist($subject);
                if (in_array(Subject::TYPE_PRACTICE, $types)) {
                    $labTeacher = $form->get("teacher_labs")->getData();
                    $labSubject = clone($subject);
                    $labSubject->setType(Subject::TYPE_PRACTICE);
                    $labSubject->setTeacher($labTeacher);
                    $labSubject->setParent($subject);
                    $em->persist($labSubject);
                }
            } else {
                $types = $form->get("type")->getData();
                if (in_array(Subject::TYPE_PRACTICE, $types)) {
                    $labTeacher = $form->get("teacher_labs")->getData();
                    if (is_null($saveSubject->getChildren())) {
                        $labSubject = clone($subject);
                        $labSubject->setType(Subject::TYPE_PRACTICE);
                        $labSubject->setTeacher($labTeacher);
                        $labSubject->setParent($subject);
                        $em->persist($labSubject);
                    } else {
                        $saveSubject->getChildren()->setTeacher($labTeacher);
                    }

                } else {
                    if (!is_null($saveSubject->getChildren())) {
                        $em->remove($saveSubject->getChildren());
                    }
                }
            }
            $em->flush();

            return new FormAcceptedResponse();
        }

        return $this->render("@App/subjectForm.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/subject/byTeacher/{id}", name="subject_list_by_teacher")
     */
    public function getSubjectListAction(Request $request, Teacher $teacher)
    {
        $subjects = $this->getDoctrine()->getRepository("AppBundle:Subject")->findBy(
            [
                "teacher" => $teacher,
                "user"    => $this->getUser()
            ]
        );

        return $this->render("@App/subjectList.html.twig", [
            "subjects" => $subjects
        ]);
    }

    /**
     * @param Subject $subject
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/subject/view/{id}", name="subject_view")
     */
    public function showAction(Subject $subject)
    {
        return $this->render("@App/subjectView.html.twig", [
            "subject" => $subject
        ]);
    }


    /**
     * @param Subject $subject
     * @Route("/subject/preview/{id}", name="subject_preview")
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function getSubjectInfoPage(Subject $subject)
    {
        return new Response($this->renderView("@App/subjectPreview.html.twig", [
            "subject" => $subject
        ]));
    }

    /**
     * @Route("/event/preview/{id}", name="event_preview")
     * @param Event $event
     * @return Response
     */
    public function getEventInfoPage(Event $event)
    {
        return new Response($event->getDescription());
    }

    /**
     * @param Subject $subject
     * @Route("/subject/remove/{id}", name="subject_remove")
     * @return FormAcceptedResponse
     * @throws \LogicException
     */
    public function remove(Subject $subject)
    {
        $this->getDoctrine()->getManager()->remove($subject);
        $this->getDoctrine()->getManager()->flush();

        return new FormAcceptedResponse();
    }
}