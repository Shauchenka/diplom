<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 20.04.17
 * Time: 16:37
 */

namespace AppBundle\Controller;


use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\Schedule;
use AppBundle\Entity\Subject;
use AppBundle\Form\ScheduleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ScheduleController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class ScheduleController extends Controller
{
    /**
     * @Route("/schedule/{id}", name="schedule", requirements={ "id": "\d+" })
     */
    public function indexAction(Request $request, Subject $subject)
    {
        $schedule = new Schedule();

        $schedule->setSubject($subject);

        $form = $this->createForm(new ScheduleType(), $schedule, [
            "action" => $this->generateUrl("schedule", [
                "id" => $subject->getId()
            ])
        ]);

        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $schedule = $form->getData();
            $schedule->setSubject($subject);
            $em->persist($schedule);
            $em->flush();

            return new FormAcceptedResponse();
        }

        return $this->render("@App/schedule/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/schedule/list/{id}", name="schedule_list")
     */
    public function listAction(Subject $subject)
    {
        $weekList = $this->getDoctrine()->getRepository("AppBundle:Schedule")->findBy([
            "subject" => $subject
        ]);

        return $this->render("@App/schedule/list.html.twig", [
            "schedules" => $weekList
        ]);
    }
}