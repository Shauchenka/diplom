/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var eventId = $("#exp_id").data("id");

    var manager = new FormManager();
    var container = $(".expFormBox .form-container");
    manager.setContainer(container);
    manager.onLoad(function () {
         $(container).find(".datepicker").datetimepicker();
    });

    if(eventId){
        // manager.setUrl(projectEnv + "//edit/" + eventId);
    } else {
        manager.setUrl(projectEnv + "/expenditure/new");
    }

    var tableManager = new TableManager();
    tableManager.setContainer($(".expListBox .box-body"));
    tableManager.setUrl( projectEnv + "/expenditure/list");
    $(".expListBox .box-body").off().on("click", ".exp-remove", function () {
        removeEvent($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();

    manager.onSuccessSubmit(function () {
        tableManager.init();
    });

    manager.init();

});

function removeEvent(id, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/expenditure/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}