<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 11.04.2017
 * Time: 22:08
 */

namespace CalendarBundle\Component;


use Symfony\Component\Yaml\Yaml;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpKernel\Config\FileLocator;
use SbS\AdminLTEBundle\Model\MenuItemModel;

class MenuBuilder
{
    /**
     * @var array
     */
    private $menuArray;
    /**
     * @var FileLocator
     */
    private $fileLocator;
    /**
     * @var Router
     */
    private $router;

    /**
     * MenuBuilder constructor.
     * @param FileLocator $file_locator
     * @param Router $router
     */
    public function __construct(FileLocator $file_locator, Router $router)
    {
        $this->fileLocator = $file_locator;
        $this->router      = $router;
//        $this->reader      = $reader;
    }
    /**
     * @return array
     */
    public function getMenu()
    {
        $treeMenu = $this->getMenuTree();
        return $this->buildMenu($treeMenu);
    }
    /**
     * @return array|mixed
     */
    private function getMenuTree()
    {
        $resource        = $this->fileLocator->locate("@CalendarBundle/Resources/config/menu.yml");
        $this->menuArray = Yaml::parse(file_get_contents($resource));
        return $this->menuArray;
    }
    /**
     * @param $tree
     * @return array
     */
    private function buildMenu($tree)
    {
        $retData = [];
        foreach ($tree as $row) {
            $menu = new MenuItemModel($row["label"]);
            if (isset($row["children"])) {
                $menu->setChildren($this->buildMenu($row["children"]));
                if (empty($menu->getChildren())) {
                    continue;
                }
            }
            if (isset($row["route"])) {
                if (strpos($row["route"], "/")) {
                    $menu->setRoute($row["route"]);
                } elseif ($this->checkRoleOnRoute($row["route"])) {
                    $menu->setRoute($row["route"]);
                } else {
                    continue;
                }
            }
            if (isset($row["icon"])) {
                $menu->setIcon($row["icon"]);
            }
            if (isset($row["badges"])) {
                $menu->setBadges($row["badges"]);
            }
            $retData[] = $menu;
        }
        return $retData;
    }
    /**
     * @param $route
     * @return bool
     */
    private function checkRoleOnRoute($route)
    {
        try {
            $route = $this->router->getRouteCollection()->get($route);
            if ($route === null) {
                throw new \Exception("Route does not exist in system.");
            }
            $action     = $route->getDefault('_controller');
            $controller = explode("::", $action);
//            if ($this->reader->checkAccess($controller[0], $controller[1])) {
                return true;
//            }
        } catch (\Exception $ex) {
            return false;
        }
    }
}