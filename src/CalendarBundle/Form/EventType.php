<?php

namespace CalendarBundle\Form;

use CalendarBundle\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Event $data */
        $data = $builder->getData();

        $builder
            ->add('name', null, [
                "label" => "Название"
            ])
            ->add('description', TextareaType::class, [
                "label" => "Описание"
            ])
            ->add('startDate', TextType::class, [
                "data"  => !is_null($data->getStartDate()) ? $data->getStartDate()->format("Y-m-d H:i:s") : "",
                "label" => "Дата начала",
                "attr"  => [
                    "class" => "datepicker"
                ]
            ])
            ->add('endDate', TextType::class, [
                "data"  => !is_null($data->getEndDate()) ? $data->getEndDate()->format("Y-m-d H:i:s") : "",
                "label" => "Дата завершения",
                "attr"  => [
                    "class" => "datepicker"
                ]
            ])
            ->add("save", SubmitType::class, [
                "label" => "Добавить",
                "attr" => [
                    "class" => "btn btn-danger pull-right"
                ]
            ]);
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CalendarBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'calendarbundle_event';
    }


}
