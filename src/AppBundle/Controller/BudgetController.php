<?php

namespace AppBundle\Controller;

use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\Budget;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Budget controller.
 *
 * @Route("budget")
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class BudgetController extends Controller
{
    /**
     * Lists all budget entities.
     *
     * @Route("/list", name="budget_index")
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $budgets = $em->getRepository('AppBundle:Budget')->findBy([
            'user' => $this->getUser()
        ]);

        return $this->render('@App/budget/index.html.twig', array(
            'budgets' => $budgets,
        ));
    }

    /**
     * Lists all budget entities.
     *
     * @Route("/monthList/{month}", name="budget_month")
     * @Method("GET")
     */
    public function monthListAction($month)
    {
        $em = $this->getDoctrine()->getManager();

        $budgets = $em->getRepository('AppBundle:Budget')->findBy([
            "user" => $this->getUser(),
            "month" => $month
        ]);

        return $this->render('@App/budget/index.html.twig', array(
            'budgets' => $budgets,
        ));
    }

    /**
     * @param Budget|null $budget
     * @Route("/page", name="budget_page")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction(Budget $budget = null)
    {
        return $this->render("@App/budget/page.html.twig", [
            "id" => is_null($budget) ? null : $budget->getId()
        ]);
    }


    /**
     * Creates a new budget entity.
     *
     * @Route("/new", name="budget_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $budget = new Budget();
        $form = $this->createForm('AppBundle\Form\BudgetType', $budget, [
            "action" => $this->generateUrl("budget_new")
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($budget);
            $em->flush();

            return new FormAcceptedResponse();
        }

        return $this->render('@App/budget/new.html.twig', array(
            'budget' => $budget,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a budget entity.
     *
     * @Route("/{id}", name="budget_show")
     * @Method("GET")
     */
    public function showAction(Budget $budget)
    {
        $deleteForm = $this->createDeleteForm($budget);

        return $this->render('@App/budget/show.html.twig', array(
            'budget' => $budget,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing budget entity.
     *
     * @Route("/{id}/edit", name="budget_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Budget $budget)
    {
        $deleteForm = $this->createDeleteForm($budget);
        $editForm = $this->createForm('AppBundle\Form\BudgetType', $budget);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('budget_edit', array('id' => $budget->getId()));
        }

        return $this->render('@App/budget/edit.html.twig', array(
            'budget' => $budget,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a budget entity.
     *
     * @Route("/remove/{id}", name="budget_delete")
     */
    public function deleteAction(Request $request, Budget $budget)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($budget);
        $em->flush();

        return new FormAcceptedResponse();
    }

    /**
     * Creates a form to delete a budget entity.
     *
     * @param Budget $budget The budget entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Budget $budget)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('budget_delete', array('id' => $budget->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
