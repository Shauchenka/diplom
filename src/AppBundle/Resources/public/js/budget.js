/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var eventId = $("#budget_id").data("id");

    var manager = new FormManager();
    var container = $(".budgetFormBox .form-container");
    manager.setContainer(container);
    manager.onLoad(function () {
         $(container).find(".datepicker").datetimepicker();
    });

    if(eventId){
        // manager.setUrl(projectEnv + "//edit/" + eventId);
    } else {
        manager.setUrl(projectEnv + "/budget/new");
    }

    var tableManager = new TableManager();
    tableManager.setContainer($(".budgetListBox .box-body"));
    tableManager.setUrl( projectEnv + "/budget/list");
    $(".budgetListBox .box-body").off().on("click", ".budget-remove", function () {
        removeEvent($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();

    manager.onSuccessSubmit(function () {
        tableManager.init();
    });

    manager.init();

});

function removeEvent(id, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/budget/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}