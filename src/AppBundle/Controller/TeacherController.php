<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 12.04.17
 * Time: 16:11
 */

namespace AppBundle\Controller;



use AppBundle\Component\ExportManager;
use AppBundle\Component\Response\FileResponse;
use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\Schedule;
use AppBundle\Entity\Subject;
use AppBundle\Entity\Teacher;
use AppBundle\Entity\User;
use AppBundle\Form\TeacherType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class TeacherController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class TeacherController extends Controller
{
    /**
     * @Route("/teacher", name="teacher_form")
     */
    public function indexAction(Request $request)
    {
        return $this->render("@App/teacher.html.twig");
    }

    /**
     * @Route("/teacher/{id}", name="teacher_form_with_id", requirements={ "id": "\d+" })
     */
    public function teacherAction(Request $request, Teacher $teacher)
    {
        return $this->render("@App/teacher.html.twig", [
            "id" => $teacher->getId()
        ]);
    }

        /**
     * @Route("/teacher/page", name="teacher_page")
     */
    public function teacherListPageAction(Request $request)
    {
        return $this->render("@App/teacherListPage.html.twig");
    }


    /**
     * @param Request $request
     * @param Teacher $teacher
     * @return \Symfony\Component\Form\FormView
     * @Route("/teacher/add", name="teacher_add")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(new TeacherType(), new Teacher(), [
            "action" => $this->generateUrl("teacher_add")
        ]);
        $form->handleRequest($request);

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return new FormAcceptedResponse();
        }

        return $this->render("@App/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Teacher $teacher
     * @return \Symfony\Component\Form\FormView
     * @Route("/teacher/load/{id}", name="teacher_load_with_id")
     */
    public function loadAction(Request $request, Teacher $teacher)
    {
        $action = $this->generateUrl("teacher_load_with_id", [
            "id" => $teacher->getId()
        ]);

        $form = $this->createForm(new TeacherType(), $teacher, [
            "action" => $action
        ]);

        $form->handleRequest($request);

        if($form->isValid()){
            $this->getDoctrine()->getManager()->flush();
            return new FormAcceptedResponse();
        }

        return $this->render("@App/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/teacher/list", name="teacher_list")
     */
    public function getTeacherListAction(Request $request)
    {
        $teachers = $this->getDoctrine()->getRepository("AppBundle:Teacher")->findBy([
            "user" => $this->getUser()
        ]);

        return $this->render("@App/teacherList.html.twig", [
            "teacherList" => $teachers
        ]);
    }


    /**
     * @param Teacher $teacher
     * @Route("/teacher/remove/{id}", name="teacher_remove")
     * @return FormAcceptedResponse
     * @throws \LogicException
     */
    public function remove(Teacher $teacher)
    {
        $this->getDoctrine()->getManager()->remove($teacher);
        $this->getDoctrine()->getManager()->flush();

        return new FormAcceptedResponse();
    }


    /**
     * @param Teacher $teacher
     * @Route("/teacher/test", name="teacher_test")
     * @return Response
     * @throws \LogicException
     */
    public function test()
    {
        $this->cloneUserSched();
    }

    private function cloneUserSched()
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("AppBundle:User")->find(9);
        $current = $this->getDoctrine()->getRepository("AppBundle:User")->find(7);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $teachers = $em->getRepository("AppBundle:Teacher")->findBy([
            "user" => $current
        ]);

        //REMOVE OLD DATA
        foreach ($teachers as $teacher){
            $em->remove($teacher);
        }
        $em->flush();

        $newTeachers =  $em->getRepository("AppBundle:Teacher")->findBy([
            "user" => $user
        ]);

        $parent = null;

        foreach ($newTeachers as $teacher){
            $newTeacher = clone $teacher;
            $newTeacher->setUser($current);

            /** @var Subject $subject */
            foreach($newTeacher->getSubjects() as $subject){
                /** @var Subject $newSubject */
                $newSubject = clone $subject;

                if(!is_null($newSubject->getChildren())){
                    $parent = $newSubject;
                }

                if(!is_null($newSubject->getParent())){
                    $newSubject->setParent($parent);
                    $parent = null;
                }

                $newSubject->setTeacher($newTeacher);
                $newSubject->setUser($current);
                $em->persist($newSubject);

                foreach ($subject->getSchedule() as $schedule){
                    /** @var Schedule $newSchedule */
                    $newSchedule = clone  $schedule;

                    $newSchedule->setUser($current);
                    $newSchedule->setSubject($newSubject);
                    $em->persist($newSchedule);
                }
            }

            $em->persist($newTeacher);
        }
        $em->flush();

    }



}