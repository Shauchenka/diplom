<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 04.05.17
 * Time: 13:24
 */

namespace AppBundle\EventListener;


use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserLifecycleListener implements EventSubscriber
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist => "prePersist"
        ];
    }


    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if(method_exists($entity, "setUser")){
            $user = $this->container->get('security.context')->getToken()->getUser();
            $entity->setUser($user);
        }
    }

}