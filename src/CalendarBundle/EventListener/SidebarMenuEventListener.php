<?php

namespace CalendarBundle\EventListener;
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 11.04.2017
 * Time: 22:13
 */

use CalendarBundle\Component\MenuBuilder;
use SbS\AdminLTEBundle\Event\SidebarMenuEvent;

class SidebarMenuEventListener
{
    private $menu;
    /**
     * SidebarMenuEventListener constructor.
     * @param MenuBuilder $menu
     */
    public function __construct(MenuBuilder $menu)
    {
        $this->menu = $menu->getMenu();
    }
    /**
     * @param SidebarMenuEvent $event
     */
    public function onShowMenu(SidebarMenuEvent $event)
    {
        foreach ($this->menu as $item) {
            $event->addItem($item);
        }
    }
}