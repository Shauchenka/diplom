<?php

namespace AppBundle\Form;

use AppBundle\Entity\Expenditure;
use CalendarBundle\Component\MonthConstant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ExpenditureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $numOfMonth = new \DateTime();

        $numOfMonthValue = (int)$numOfMonth->format("m");
        $builder
            ->add('name', null, [
                "label" => "Название"
            ])
            ->add('sum', null, [
                "label" => "Сумма"
            ])
            ->add('type', ChoiceType::class, [
                "choices" => Expenditure::$types,
                "label"   => "Тип"
            ])
            ->add('description', TextareaType::class, [
                "label" => "Описание"
            ])
            ->add('month', ChoiceType::class, [
                "choices" => MonthConstant::$month,
                "data"    => $numOfMonthValue,
                "label"   => "Месяц"
            ])
            ->add("save", SubmitType::class, [
                "label" => "Добавить",
                "attr" => [
                    "class" => "btn btn-danger pull-right"
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Expenditure::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_expenditure';
    }


}
