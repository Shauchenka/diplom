<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10.05.17
 * Time: 20:25
 */

namespace AppBundle\Component;


class ColorsConstant
{
    private static $colors = [
        "#000000",
        "#ffffff",
        "#ffc0cb",
        "#008080",
        "#ffe4e1",
        "#ff0000",
        "#ffd700",
        "#00ffff",
        "#d3ffce",
        "#ff7373",
        "#40e0d0",
        "#0000ff",
        "#eeeeee",
        "#e6e6fa",
        "#ffa500",
        "#b0e0e6",
        "#cccccc",
        "#f0f8ff",
        "#7fffd4",
        "#333333",
        "#00ff00",
        "#c0c0c0",
        "#f6546a",
        "#800080",
        "#003366",
        "#c6e2ff",
        "#666666",
        "#fa8072"
    ];


    /**
     * @param $id
     * @return mixed
     */
    public static function getColor($id)
    {
        while(count(self::$colors)-1 > $id){
            $id -= count(self::$colors)-1;
        }

        return self::$colors[$id];
    }
}