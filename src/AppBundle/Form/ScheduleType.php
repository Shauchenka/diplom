<?php

namespace AppBundle\Form;

use AppBundle\Entity\Schedule;
use CalendarBundle\Component\WeekConstant;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScheduleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dowList = WeekConstant::$daysOfWeek;
        array_pop($dowList);
        $builder
            ->add('dow', ChoiceType::class, [
                "label" => "День недели",
                "choices" => $dowList,
                "expanded" => true,
                "multiple" => false,
                "attr" => [
                    "class" => "btn-group",
                    "data-toggle" => "buttons"
                ]
            ])
            ->add('number', ChoiceType::class, [
                "label" => "Номер пары",
                "choices" => range(1, 8),
                "expanded" => true,
                "multiple" => false,
                "attr" => [
                    "class" => "btn-group",
                    "data-toggle" => "buttons"
                ]
            ])
            ->add('weekTypeForm', ChoiceType::class, [
                "mapped" => false,
                "label" => "Тип недели",
                "choices" => WeekConstant::$weekType,
                "expanded" => true,
                "multiple" => true,
                "attr" => [
                    "class" => "btn-group",
                    "data-toggle" => "buttons"
                ]
            ])
            ->add('auditory', null, [
                "label" => "Аудитория",
            ])
            ->add("save", SubmitType::class, [
                "label" => "Добавить",
                "attr" => [
                    "class" => "btn btn-danger pull-right"
                ]
            ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Schedule $schedule */
            $schedule = $event->getData();
            $weekTypes = $event->getForm()->get("weekTypeForm")->getData();

            if(count($weekTypes) == 2){
                $weekType = 2;
            } else {
                $weekType = $weekTypes[0];
            }

            $schedule->setWeekType($weekType);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Schedule'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_schedule';
    }


}
