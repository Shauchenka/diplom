<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 *
 */
class DefaultController extends Controller
{
    /**
     * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $subjects = $this->get("doctrine")->getRepository("AppBundle:Subject")->getSubjectsWithDebt($this->getUser());

        $todaySubjects = $this->get("doctrine")->getRepository("AppBundle:Subject")->getSubjectsWithDebtByDate($this->getUser(), new \DateTime());
        $date = new \DateTime();
        $date->modify("+1 day");
        $tomorrowSubjects = $this->get("doctrine")->getRepository("AppBundle:Subject")->getSubjectsWithDebtByDate($this->getUser(), $date);


        return $this->render('default/index.html.twig', array(
            'subjects' => $subjects,
            "today"    => $todaySubjects,
            "tomorrow" => $tomorrowSubjects
        ));
    }

    /**
     * @Route("/welcome", name="welcome")
     */
    public function welcomeAction()
    {
        return $this->render("default/welcome.html.twig");
    }
}
