<?php

namespace AppBundle\EventListener;

/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 24.04.17
 * Time: 16:33
 */
use SbS\AdminLTEBundle\Event\UserEvent;
use SbS\AdminLTEBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserEventListener
{
    private $token_storage;

    public function __construct(TokenStorageInterface $token_storage)
    {
        $this->token_storage = $token_storage;
    }

    public function onShowUser(UserEvent $event)
    {
        /** @var UserInterface $user */
        $user = $this->token_storage->getToken()->getUser();

        $event->setUser($user);
    }
}