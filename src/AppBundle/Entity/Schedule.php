<?php

namespace AppBundle\Entity;

use CalendarBundle\Component\WeekConstant;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Schedule
 *
 * @ORM\Table(name="schedule")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ScheduleRepository")
 */
class Schedule
{
    /**
     * @var int
     *
     * @ORM\Column(name="Sch_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Subject", inversedBy="schedule")
     * @ORM\JoinColumn(name="Sch_Sbj_Id", referencedColumnName="Sbj_Id")
     */
    private $subject;

    /**
     * @var integer
     * @ORM\Column(name="Sch_Dow", type="integer")
     * @Assert\Range(
     *     min="0",
     *     max="5"
     *     )
     */
    private $dow;

    /**
     * @var integer
     * @ORM\Column(name="Sch_Week_Type", type="integer")
     * @Assert\Range(
     *     min="0",
     *     max="2"
     *     )
     */
    private $weekType;

    /**
     * @var integer
     * @ORM\Column(name="Sch_Number", type="integer")
     * @Assert\Range(
     *     min="1",
     *     max="8"
     *     )
     */
    private $number;


    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="Sch_Student_Id")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="Sch_Auditory", type="string")
     */
    private $auditory;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param \AppBundle\Entity\Subject $subject
     *
     * @return Schedule
     */
    public function setSubject(\AppBundle\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \AppBundle\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Schedule
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set dow
     *
     * @param integer $dow
     *
     * @return Schedule
     */
    public function setDow($dow)
    {
        $this->dow = $dow;

        return $this;
    }

    /**
     * Get dow
     *
     * @return integer
     */
    public function getDow()
    {
        return $this->dow;
    }

    /**
     * Set weekType
     *
     * @param integer $weekType
     *
     * @return Schedule
     */
    public function setWeekType($weekType)
    {
        $this->weekType = $weekType;

        return $this;
    }

    /**
     * Get weekType
     *
     * @return integer
     */
    public function getWeekType()
    {
        return $this->weekType;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Schedule
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getDowName()
    {
        return WeekConstant::$daysOfWeek[$this->dow];
    }

    /**
     * @return string
     */
    public function getWeekTypeName()
    {
        if (array_key_exists($this->weekType, WeekConstant::$weekType)) {
            return WeekConstant::$weekType[$this->weekType];
        }

        return implode(", ", WeekConstant::$weekType);
    }


    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Schedule
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set auditory
     *
     * @param string $auditory
     *
     * @return Schedule
     */
    public function setAuditory($auditory)
    {
        $this->auditory = $auditory;

        return $this;
    }

    /**
     * Get auditory
     *
     * @return string
     */
    public function getAuditory()
    {
        return $this->auditory;
    }
}
