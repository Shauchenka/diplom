<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 11.05.17
 * Time: 18:45
 */

namespace AppBundle\Component;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected $container;

    protected $security;

    public function __construct(ContainerInterface $container, SecurityContext $security)
    {
        $this->container = $container;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $this->security->getToken()->getUser();
        if ($user->getAlgorithm() === 'sha1') {
            $password = $request->get('_password');
            $salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);

            $user->setPlainPassword($password);
            $user->setSalt($salt);

            $userManager = $this->container->get('fos_user.user_manager');
            $userManager->updateUser($user);
        }
        $response = new RedirectResponse('/');

        return $response;
    }

}