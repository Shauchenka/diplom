<?php

namespace CalendarBundle\Controller;

use CalendarBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="dashboard_main")
     */
    public function indexAction()
    {
        $date = new \DateTime();
        $data = $this->get("calendar.builder")->getData(($date));
        $userData = $this->get("calendar.builder")->getUserData(($date));

        return $this->render('CalendarBundle::index.html.twig', [
            "data"      => $data,
            "userData"  => $userData
        ]);
    }

}
