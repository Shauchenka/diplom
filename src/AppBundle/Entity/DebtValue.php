<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 * DebtValue
 *
 * @ORM\Table(name="debt_value")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DebtValueRepository")
 */
class DebtValue
{

    const GRADE_BOOL    = 0;
    const GRADE_NUMERIC = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="Dbv_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Debt", inversedBy="id")
     * @ORM\JoinColumn(name="Dbv_Dbt_Id", referencedColumnName="Dbt_Id")
     */
    private $debt;

    /**
     * @var integer
     *
     * @ORM\Column(name="Dbv_Grade", type="integer", options={"default": 0})
     */
    private $grade = self::GRADE_BOOL;

    /**
     * @var integer
     *
     * @ORM\Column(name="Dbv_Value", type="integer", options={"default": 0})
     */
    private $value= 0;

    /**
     * @var string
     * @ORM\Column(name="Dbv_Additional", type="string", options={"default": ""})
     */
    private $additional = "";

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="Dbv_Date_Modified", type="datetime")
     */
    private $dateModified;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="Dbv_Date_Added", type="datetime")
     */
    private $dateAdded;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="Dbv_Student_Id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     *
     * @return DebtValue
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return integer
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return DebtValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set additional
     *
     * @param string $additional
     *
     * @return DebtValue
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get additional
     *
     * @return string
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Set debt
     *
     * @param \AppBundle\Entity\Debt $debt
     *
     * @return DebtValue
     */
    public function setDebt(\AppBundle\Entity\Debt $debt = null)
    {
        $this->debt = $debt;

        return $this;
    }

    /**
     * Get debt
     *
     * @return \AppBundle\Entity\Debt
     */
    public function getDebt()
    {
        return $this->debt;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return DebtValue
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return DebtValue
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return DebtValue
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
