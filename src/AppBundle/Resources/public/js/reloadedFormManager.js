/**
 * Created by tshauchenka on 13.04.17.
 */
ReloadedFormManager = function () {
    FormManager.apply(this);
    this.reloadElement = null;

};

ReloadedFormManager.prototype = Object.create(FormManager.prototype);
ReloadedFormManager.prototype.constructor = ReloadedFormManager;

ReloadedFormManager.prototype.setReloadElement = function (element) {
    this.reloadElement = element;
};

ReloadedFormManager.prototype.init = function () {
    this.loadForm();
    this.registerEvents(this);

};

ReloadedFormManager.prototype.reload = function (reloadId) {
    var url = this.url + "/" + reloadId;

    var self = this;
    this.container.closest(".box").find(".overlay").show();

    this.container.load(url, function () {
        self.container.closest(".box").find(".overlay").hide();
        $(".select2").select2();

        if (self.onLoadEvent != null) {
            self.onLoadEvent();
        }
    });
};


ReloadedFormManager.prototype.registerEvents = function () {
    FormManager.prototype.registerEvents(this);
    var self = this;
    $(this.container).on("change", this.reloadElement, function () {
        self.reload(this.value);
    });
};









