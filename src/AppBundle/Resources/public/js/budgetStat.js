/**
 * Created by tshauchenka on 12.04.17.
 */

var activeMonth;

var monthList = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
];

$(document).ready(function () {
    activeMonth = parseInt($("#monthId").val());
    showMonth(activeMonth);

    $(".next-month, .prev-month").on("click", function () {
        var nextMonth = parseInt($(this).data("month"));
        showMonth(nextMonth);
        updateBtns(nextMonth);
    });

    $(document).on("click", ".paid", function () {
        var id = $(this).data("id");
        var val = $(this).prop("checked");
        updateClosedStatus(id, val);
        showMonth(activeMonth);

    });


});

function updateBtns(currentMonth) {
    $("#monthId").val(currentMonth);
    var next = $(".next-month");
    var prev = $(".prev-month");

    if (currentMonth + 1 <= 12) {
        next.prop("disabled", false);
        next.data("month", currentMonth + 1);
    } else {
        next.prop("disabled", true);
    }
    if (currentMonth - 1 >= 1) {
        prev.prop("disabled", false);
        prev.data("month", currentMonth - 1);
    } else {
        prev.prop("disabled", true);
    }

    $(".currentMonthName").text(monthList[currentMonth - 1]);

}

function showMonth(month) {
    getBudgetStat(month);
    getExpStat(month);
    getTotalStat(month);
    getExpRatioStat(month);
    getBalance(month);
    initBudgetList(month);
    initExpList(month);
    getBalanceValue(month);

}


function getBudgetStat(month) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget_stat/budget/" + month,
    })
        .done(function (data) {
            buildDebtStat('budgetContainer', data);
        });
}

function getExpStat(month) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget_stat/exp/" + month,
    })
        .done(function (data) {
            buildDebtStat('expContainer', data);
        });

}

function getExpRatioStat(month) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget_stat/expRatio/" + month,
    })
        .done(function (data) {
            buildDebtStat('expRatioContainer', data);
        });

}


function getTotalStat(month) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget_stat/ratio/" + month,
    })
        .done(function (data) {
            buildBarStat('ratioContainer', data);
        });

}


function getBalance(month) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget_stat/balance/" + month,
    })
        .done(function (data) {
            buildProgressBar('balanceContainer', data);
        });

}

function getBalanceValue(month) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget_stat/balanceValue/" + month,
    })
        .done(function (data) {
            $(".currentBudget").text(data);
        });

}

function clearContainer(container) {
    $("#" + container).empty();
}

function buildDebtStat(container, data) {
    clearContainer(container);
    new Morris.Donut({
        element: container,
        data: data
    });
}

function buildBarStat(container, data) {
    clearContainer(container);
    new Morris.Bar({
        element: container,
        data: data,
        xkey: 'm',
        ykeys: ['a', 'b'],
        labels: ['Доход', 'Расход']
    });
}

function buildProgressBar(container, data) {
    // clearContainer(container);
    container = $("#" + container);
    $(container).find(".progress-bar").attr("aria-valuenow", data);
    $(container).find(".progress-bar").text(data + "%");
    $(container).find(".progress-bar").css("width", data + "%");
}

function initBudgetList(month) {
    var tableManager = new TableManager();
    tableManager.setContainer($(".budgetListBox .box-body"));
    tableManager.setUrl(projectEnv + "/budget/monthList/" + month);
    $(".budgetListBox .box-body").off().on("click", ".budget-remove", function () {
        removeEvent($(this).data("id"), function () {
            showMonth(month);
        });
    });
    tableManager.init();
}

function initExpList(month) {
    var tableManager = new TableManager();
    tableManager.setContainer($(".expListBox .box-body"));
    tableManager.setUrl(projectEnv + "/expenditure/monthList/" + month);
    $(".expListBox .box-body").off()
        .on("click", ".exp-remove", function () {
            removeExp($(this).data("id"), function () {
                showMonth(month);
            });
        });
    tableManager.init();
}

function removeEvent(id, callback) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/budget/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}

function removeExp(id, callback) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/expenditure/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}


function updateClosedStatus(id, val) {
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/expenditure/closed/" + id + "/" + val,
    })
        .done(function () {
            main.notificationManager.successNotify("Успешно!");
        })
}
