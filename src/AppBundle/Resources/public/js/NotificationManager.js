/**
 * Created by tshauchenka on 13.04.17.
 */

NotificationManager = function() {
};


// Notification types
NotificationManager.prototype.types = {
    success: "success",
    info:    "info",
    notice:  "notice",
    error:   "error"
};
NotificationManager.prototype.title = "Успешно";

// Default title and message
NotificationManager.prototype.text  = "Сохранено";

NotificationManager.prototype.notify = function(title, text, type){
    new PNotify({
        title: title,
        text: text,
        type: type,
        styling: "bootstrap3"
    });
};

NotificationManager.prototype.successNotify = function(message){
    this.notify("Успешно", message, this.types.success);
};

NotificationManager.prototype.errorNotify = function(message){
    this.notify("Ошибка", message, this.types.error);
};

NotificationManager.prototype.infoNotify = function(message){
    this.notify("Информация", message, this.types.success);
};