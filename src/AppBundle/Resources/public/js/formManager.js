/**
 * Created by tshauchenka on 13.04.17.
 */

FormManager = function () {
    this.url = null;
    this.container = null;
    this.onBeforeSubmitEvent = null;
    this.onSuccessSubmitEvent = null;
    this.onLoadEvent = null;
    this.onRegisterEventsEvent = null;
};

FormManager.prototype.setUrl = function (url) {
    this.url = window.location.origin + "/" + url;
};

FormManager.prototype.setContainer = function (container) {
    this.container = container;
};

FormManager.prototype.getUrl = function () {
    return this.url;
};

FormManager.prototype.getContainer = function () {
    return this.container;
};

FormManager.prototype.onBeforeSubmit = function (callback) {
    this.beforeSubmitEvent = callback;
};

FormManager.prototype.onSuccessSubmit = function (callback) {
    this.onSuccessSubmitEvent = callback;
};

FormManager.prototype.onLoad = function (callback) {
    this.onLoadEvent = callback;
};

FormManager.prototype.onRegisterEvents = function (callback) {
    this.onRegisterEventsEvent = callback;
};


FormManager.prototype.loadForm = function () {
    var self = this;
    this.container.closest(".box").find(".overlay").show();

    this.container.load(this.url, function () {
        self.container.closest(".box").find(".overlay").hide();
        $(".select2").select2();

        if(self.onLoadEvent != null){
            self.onLoadEvent();
        }
    });
};

FormManager.prototype.init = function () {
    this.loadForm();
    this.registerEvents();

};

FormManager.prototype.registerEvents = function () {
    var self = this;

    var options = {
        beforeSubmit: beforeSubmit,
        success: successSubmit,
        error: errorSubmit,
        clearForm: true

    };

    $(this.container).off().on("submit", "form", function () { //[data-ajax='submit']
        $(this).ajaxSubmit(options);

        return false;
    });

    function successSubmit  (responseText, statusText, response, $form) {

        if(response.status == 202){
            main.notificationManager.successNotify("Сохранено");
            if(self.onSuccessSubmitEvent){
                self.onSuccessSubmitEvent();
            } else {
                self.loadForm();
            }
            return true;
        }
        $($form).closest(self.container).html(responseText);
        $(".select2").select2();

    }

    function errorSubmit(response, status, xhr) {
        main.notificationManager.notify("Ошибка", xhr, main.notificationManager.types.error);
    };

    function beforeSubmit() { //TODO: add params
        if(self.onBeforeSubmitEvent != null){
            self.onBeforeSubmitEvent();
        }
    };

    if(self.onRegisterEventsEvent != null){
        self.onRegisterEventsEvent();
    }
};

FormManager.prototype.reload = function (reloadId) {
    var url = this.url + "/" + reloadId;

    var self = this;
    this.container.closest(".box").find(".overlay").show();

    this.container.load(url, function () {
        self.container.closest(".box").find(".overlay").hide();
        $(".select2").select2();

        if (self.onLoadEvent != null) {
            self.onLoadEvent();
        }
    });
};

