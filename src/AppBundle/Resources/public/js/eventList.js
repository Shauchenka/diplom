/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var tableManager = new TableManager();
    tableManager.setContainer($(".eventListBox .box-body"));
    tableManager.setUrl( projectEnv + "/event/list");
    $(".eventListBox .box-body").off().on("click", ".subject-remove", function () {
        removeEvent($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();
});

function removeEvent(id, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/event/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}