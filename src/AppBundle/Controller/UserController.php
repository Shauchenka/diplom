<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 14.05.2017
 * Time: 23:43
 */

namespace AppBundle\Controller;

use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * @Route("/userlist", name="user_list")
     */
    public function sharedUserList(){


        return $this->render("@App/userListPage.html.twig");
    }

    /**
     * @Route("/userlist/list", name="user_list_list")
     */
    public function listAction()
    {
        $users = $this->getDoctrine()->getRepository("AppBundle:User")->findBy([
            "shareSched" => 1
        ]);

        return $this->render("@App/userList.html.twig", [
            "users" => $users
        ]);
    }


    /**
     * @Route("/userlist/status", name="schedule_status")
     */
    public function myStatusAction()
    {
        return $this->render("@App/userStatus.html.twig", [
            "user" => $this->getUser()
        ]);
    }

    /**
     * @Route("/userlist/sharestatus", name="update_status")
     */
    public function updateShareStatusAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        
        if($user->getShareSched()){
            $user->setShareSched(0);    
        } else {
            $user->setShareSched(1);
        }
        
        $this->getDoctrine()->getManager()->flush();

        return new FormAcceptedResponse();
    }
    
    /**
     * @Route("/userlist/apply/{id}", name="sched_apply")
     */
    public function applyToMeAction(User $user)
    {
        $this->cloneUserSched($user);
        
       return new FormAcceptedResponse();
    }

    private function cloneUserSched($user)
    {
        $current = $this->getUser();

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $teachers = $em->getRepository("AppBundle:Teacher")->findBy([
            "user" => $current
        ]);

        //REMOVE OLD DATA
        foreach ($teachers as $teacher){
            $em->remove($teacher);
        }
        $em->flush();

        $newTeachers =  $em->getRepository("AppBundle:Teacher")->findBy([
            "user" => $user
        ]);

        $parent = null;

        foreach ($newTeachers as $teacher){
            $newTeacher = clone $teacher;
            $newTeacher->setUser($current);

            /** @var Subject $subject */
            foreach($newTeacher->getSubjects() as $subject){
                /** @var Subject $newSubject */
                $newSubject = clone $subject;

                if(!is_null($newSubject->getChildren())){
                    $parent = $newSubject;
                }

                if(!is_null($newSubject->getParent())){
                    $newSubject->setParent($parent);
                    $parent = null;
                }

                $newSubject->setTeacher($newTeacher);
                $newSubject->setUser($current);
                $em->persist($newSubject);

                foreach ($subject->getSchedule() as $schedule){
                    /** @var Schedule $newSchedule */
                    $newSchedule = clone  $schedule;

                    $newSchedule->setUser($current);
                    $newSchedule->setSubject($newSubject);
                    $em->persist($newSchedule);
                }
            }

            $em->persist($newTeacher);
        }
        $em->flush();

    }
}