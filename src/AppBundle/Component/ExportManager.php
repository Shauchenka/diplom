<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 12.05.17
 * Time: 12:37
 */

namespace AppBundle\Component;


use AppBundle\Entity\Schedule;

class ExportManager
{
    /**
     * @param $array
     * @return string
     */
    public function Export($array, $path)
    {


        $xml = new \XMLWriter();
       // $path = 'c:\test.xml';
//        $xml->openURI($path);
//        $xml->startDocument();
//        $xml->setIndent(true);

        header('Content-type: text/xml; charset=UTF-8');

        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');


        $xml->startElement('schedules');
        $xml->writeAttribute('dow', "2222");

//
//        /** @var Schedule $schedule */
//        foreach ($array as $schedule) {
//            $schedule->getSubject()->getTeacher();
//            $subject = $schedule->getSubject();
//            $teacher = $subject->getTeacher();
//
//            $xml->startElement("schedule");
//                $xml->writeAttribute('dow', $schedule->getDow());
//                $xml->writeAttribute('weekType', $schedule->getWeekType());
//                $xml->writeAttribute('number', $schedule->getNumber());
//
//                $xml->startElement("subject");
//                    $xml->writeAttribute('name', $subject->getName());
//                    $xml->writeAttribute('type', $subject->getType());
//                    $xml->writeAttribute('parent', is_null($subject->getParent()) ? "0" : "1");
//
//                    $xml->startElement("teacher");
//                        $xml->writeAttribute('first', $teacher->getFirst());
//                        $xml->writeAttribute('last', $teacher->getLast());
//                        $xml->writeAttribute('patronymic', $teacher->getPatronymic());
//                    $xml->endElement();
//                $xml->endElement();
//            $xml->endElement();
//        }
        $xml->endElement();


        $xml->endDocument();
        $content =  $xml->outputMemory(TRUE);

        $oXMLout = new \XMLWriter();
        $oXMLout->openMemory();
        $oXMLout->startElement("item");
        $oXMLout->writeElement("quantity", 8);
        $oXMLout->writeElement("price_per_quantity", 110);
        $oXMLout->endElement();
        return $oXMLout->outputMemory();


        return $content;

    }
}