<?php

namespace AppBundle\Controller;

use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\Expenditure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Expenditure controller.
 *
 * @Route("expenditure")
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class ExpenditureController extends Controller
{
    /**
     * Lists all expenditure entities.
     *
     * @Route("/list", name="expenditure_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $expenditures = $em->getRepository('AppBundle:Expenditure')->findBy([
            "user" => $this->getUser(),
        ]);

        return $this->render('@App/expenditure/index.html.twig', array(
            'expenditures' => $expenditures,
        ));
    }

    /**
     * Lists all expenditure entities.
     *
     * @Route("/monthList/{month}", name="expenditure_month_list")
     * @Method("GET")
     */
    public function monthListAction($month)
    {
        $em = $this->getDoctrine()->getManager();

        $expenditures = $em->getRepository('AppBundle:Expenditure')->findBy([
            "user" => $this->getUser(),
            "month" => $month
        ]);

        return $this->render('@App/expenditure/month.html.twig', array(
            'expenditures' => $expenditures,
        ));
    }

    /**
     * @param Expenditure|null $exp
     * @Route("/page", name="expend_page")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction(Expenditure $exp = null)
    {
        return $this->render("@App/expenditure/page.html.twig", [
            "id" => is_null($exp) ? null : $exp->getId()
        ]);
    }

    /**
     * Creates a new expenditure entity.
     *
     * @Route("/new", name="expenditure_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $expenditure = new Expenditure();
        $form = $this->createForm('AppBundle\Form\ExpenditureType', $expenditure, [
            "action" => $this->generateUrl("expenditure_new")
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expenditure);
            $em->flush();

            return new FormAcceptedResponse();
        }

        return $this->render('@App/expenditure/new.html.twig', array(
            'expenditure' => $expenditure,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a expenditure entity.
     *
     * @Route("/{id}", name="expenditure_show")
     * @Method("GET")
     */
    public function showAction(Expenditure $expenditure)
    {
        $deleteForm = $this->createDeleteForm($expenditure);

        return $this->render('expenditure/show.html.twig', array(
            'expenditure' => $expenditure,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing expenditure entity.
     *
     * @Route("/{id}/edit", name="expenditure_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Expenditure $expenditure)
    {
        $deleteForm = $this->createDeleteForm($expenditure);
        $editForm = $this->createForm('AppBundle\Form\ExpenditureType', $expenditure);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('expenditure_edit', array('id' => $expenditure->getId()));
        }

        return $this->render('expenditure/edit.html.twig', array(
            'expenditure' => $expenditure,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a expenditure entity.
     *
     * @Route("/remove/{id}", name="expenditure_delete")
     * @param Expenditure $expenditure
     * @return FormAcceptedResponse
     * @throws \LogicException
     */
    public function deleteAction(Expenditure $expenditure)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($expenditure);
        $em->flush();

        return new FormAcceptedResponse();
    }

    /**
     * Creates a form to delete a expenditure entity.
     *
     * @param Expenditure $expenditure The expenditure entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Expenditure $expenditure)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expenditure_delete', array('id' => $expenditure->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param Expenditure $expenditure
     * @param $val
     * @return FormAcceptedResponse
     * @Route("/closed/{id}/{val}", name="expenditure_closed_update")
     */
    public function updateClosedValueAction(Expenditure $expenditure, $val)
    {
        $val = $val === 'true' ? true : false;
        $expenditure->setClosed($val);
        $this->getDoctrine()->getManager()->flush();

        return new FormAcceptedResponse();

    }
}
