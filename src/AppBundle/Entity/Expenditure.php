<?php

namespace AppBundle\Entity;

use CalendarBundle\Component\MonthConstant;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Expenditure
 *
 * @ORM\Table(name="expenditure")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpenditureRepository")
 */
class Expenditure
{

    const TYPE_NECESSARY = 0;
    const TYPE_DESIRED   = 1;
    const TYPE_OPTIONAL  = 2;
    const TYPE_UNPLANNED = 3;

    public static $types = [
        self::TYPE_NECESSARY => "Обязательные",
        self::TYPE_DESIRED   => "Желательные",
        self::TYPE_OPTIONAL  => "Необязательные",
        self::TYPE_UNPLANNED => "Внеплановые",
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="Exp_Id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Exp_Name", type="string", length=60)
     */
    private $name;

    /**
     * @var float
     * @ORM\Column(name="Exp_Sum", type="float")
     * @Assert\Range(min="0", max="1000000")
     */
    private $sum;

    /**
     * @var int
     * @ORM\Column(name="Exp_Type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="Exp_Description", type="text")
     */
    private $description;

    /**
     * @var int
     * @ORM\Column(name="Exp_Month", type="integer")
     */
    private $month;

    /**
     * @var bool
     * @ORM\Column(name="Exp_Closed", type="boolean", options={"default": 0})
     */
    private $closed = 0;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="id")
     * @ORM\JoinColumn(name="Exp_Student_Id")
     */
    private $user;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="Exp_Date_Added", type="datetime")
     */
    private $dateAdded;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Expenditure
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Expenditure
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return Expenditure
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @return mixed
     */
    public function getMonthName()
    {
        return MonthConstant::$month[$this->getMonth()];
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Expenditure
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Expenditure
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Expenditure
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        return self::$types[$this->getType()];
    }

    /**
     * Set sum
     *
     * @param float $sum
     *
     * @return Expenditure
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     *
     * @return Expenditure
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean
     */
    public function getClosed()
    {
        return $this->closed;
    }
}
