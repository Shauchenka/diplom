/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var tableManager = new TableManager();
    tableManager.setContainer($(".subjectListBox .box-body"));
    tableManager.setUrl( projectEnv + "/subject/list");
    $(".subjectListBox .box-body").off().on("click", ".subject-remove", function () {
        removeSubject($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();
});

function removeSubject(sbjId, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/subject/remove/" + sbjId,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}