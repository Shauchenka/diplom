/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var tableManager = new TableManager();
    tableManager.setContainer($(".userListBox .box-body"));
    tableManager.setUrl( projectEnv + "/userlist/list");
    $(".userListBox .box-body").off().on("click", ".apply-sched", function () {

        applyToMe($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();

    var userStatusManager = new TableManager();
    userStatusManager.setContainer($(".userStatusBox .box-body"));
    userStatusManager.setUrl( projectEnv + "/userlist/status");
    $(".userStatusBox .box-body").off().on("click", ".share-btn", function () {

        shareStatus(function () {
            userStatusManager.init();
            tableManager.init();
        });
    });
    userStatusManager.init();
});



function applyToMe(id, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/userlist/apply/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Применено!");
            callback();
        })
}
function shareStatus(callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/userlist/sharestatus"
    })
        .done(function () {
            main.notificationManager.successNotify("Сохранено!");
            callback();
        })
}