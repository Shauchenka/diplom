/**
 * Created by tshauchenka on 12.04.17.
 */

$(document).ready(function () {
    var tableManager = new TableManager();
    tableManager.setContainer($(".teacherListBox .box-body"));
    tableManager.setUrl( projectEnv + "/teacher/list");
    $(".teacherListBox .box-body").off().on("click", ".teacher-remove", function () {

        remove($(this).data("id"), function () {
            tableManager.init();
        });
    });
    tableManager.init();
});



function remove(id, callback) {
    $.ajax({
        url:  window.location.origin + "/" + projectEnv + "/teacher/remove/" + id,
    })
        .done(function () {
            main.notificationManager.successNotify("Удалено!");
            callback();
        })
}