/**
 * Created by tshauchenka on 13.04.17.
 */

TableManager = function () {
    this.url = null;
    this.container = null;
};

TableManager.prototype.setUrl = function (url) {
    this.url = window.location.origin + "/" + url;
};

TableManager.prototype.setContainer = function (container) {
    this.container = container;
};

TableManager.prototype.getUrl = function () {
    return this.url;
};

TableManager.prototype.getContainer = function () {
    return this.container;
};


TableManager.prototype.loadForm = function() {
    var self = this;
    this.container.closest(".box").find(".overlay").show();

    this.container.load(this.url, function () {
        self.container.closest(".box").find(".overlay").hide();
    });
};

TableManager.prototype.init = function () {
    this.loadForm();
    this.registerEvents();
};

TableManager.prototype.registerEvents = function () {
    var self = this;
};

