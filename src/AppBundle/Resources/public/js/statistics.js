/**
 * Created by tshauchenka on 04.05.17.
 */
$(document).ready(function () {
    getWeekStat();
    getMonthStat();

});

function getWeekStat(){
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/statistics/debt/week",
    })
        .done(function (data) {
            buildDebtStat('weekContainer', data);
        });
}

function getMonthStat(){
    $.ajax({
        url: window.location.origin + "/" + projectEnv + "/statistics/debt/month",
    })
        .done(function (data) {
            buildDebtStat('monthContainer', data);
        });

}

function buildDebtStat(container, data) {
    new Morris.Line({
        element: container,
        data: data,
        xkey: 'date',
        ykeys: ['lab', 'practice', 'other'],
        labels: ['Лабораторные', 'Практические', 'Остальные'],
        lineColors: ['#0090ff','#ff7777', '#fff600'],
        lineWidth: 2
    });
}