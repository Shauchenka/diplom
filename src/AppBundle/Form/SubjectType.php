<?php

namespace AppBundle\Form;

use AppBundle\Entity\Subject;
use AppBundle\Entity\Teacher;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [Subject::TYPE_LECTURE];
        $teacherLec = null;

        if(!is_null($builder->getData()) && !is_null($builder->getData()->getChildren())){
            array_push($choices, Subject::TYPE_PRACTICE);
            /** @var Subject $parent */
            $parent = $builder->getData()->getChildren();
            $teacherLec = $parent->getTeacher();

        }

        $builder
            ->add('name', null, [
                "label" => "Название"
            ])
            ->add('type', ChoiceType::class, [
                "label"    => "Типы предмета",
                "data"     => $choices,
                "mapped"   => false,
                "choices"  => Subject::$types,
                "expanded" => true,
                "multiple" => true
            ])
            ->add('teacher', EntityType::class, [
                "label" => "Преподаватель",
                "class" => Teacher::class,
                'query_builder' => function (EntityRepository $er) use ($options){
                    return $er->createQueryBuilder('tch')
                        ->where("tch.user = :user")
                        ->setParameter("user", $options["user"])
                        ->orderBy('tch.last', 'ASC');
                },
                'choice_label' => 'teacherLabel',
                "attr" => [
                    "class" => "select2"
                ]
            ])
            ->add('teacher_labs', EntityType::class, [
                "label" => "Преподаватель практических работ",
                "mapped" => false,
                "class" => Teacher::class,
                "data"  => $teacherLec,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('tch')
                        ->orderBy('tch.last', 'ASC');
                },
                'choice_label' => 'teacherLabel',
                "attr" => [
                    "class" => "select2"
                ]
            ])
            ->add("save", SubmitType::class, [
                "label" => "Сохранить",
                "attr" => [
                    "class" => "btn btn-danger pull-right"
                ]
            ])
;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Subject',
            "user"       => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_subject';
    }


}
