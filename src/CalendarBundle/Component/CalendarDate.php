<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 05.04.17
 * Time: 18:08
 */

namespace CalendarBundle\Component;


use Symfony\Component\Validator\Constraints\DateTime;

class CalendarDate extends \DateTime
{
    /**
     * @return CalendarDate
     */
    public function getWeekStart()
    {
        $dow = date("w", $this->getTimestamp());
        $dow = ($dow - 1 < 0) ? $dow + 7 : $dow - 1;

        return new CalendarDate("-$dow days");

    }

    /**
     * @return \DateTime
     */
    public function getWeekEnd()
    {
        $startDate = $this->getWeekStart();
        $interval = new \DateInterval("P6D");

        return $startDate->add($interval);
    }

    /**
     * @return \DateTime
     */
    public function getNexrWeekStart()
    {
        $startDate = $this->getWeekStart();
        $interval = new \DateInterval("P7D");

        return $startDate->add($interval);
    }

    /**
     * @param \DateTime $date
     * @return CalendarDate
     */
    public static function convert(\DateTime $date){
        return new self($date->format("Y-m-d"));
    }

    public static function getArrayWithDateKeys(\DateTime $start, \DateTime $end)
    {
        $period = new \DatePeriod(
            $start,
            new \DateInterval('P1D'),
            $end
        );

        $result = [];
        /** @var DateTime $value */
        foreach ($period as $value){
            $result[$value->format("Y-m-d")] = 0;
        }
        $result[$end->format("Y-m-d")] = 0;

        return $result;
    }
}