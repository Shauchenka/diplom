<?php

namespace AppBundle\Controller;

use AppBundle\Component\Response\FormAcceptedResponse;
use AppBundle\Entity\Debt;
use AppBundle\Entity\DebtValue;
use AppBundle\Entity\Subject;
use AppBundle\Form\DebtFormType;
use AppBundle\Form\DebtType;
use CalendarBundle\Component\CalendarDate;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class DebtController
 * @package AppBundle\Controller
 * @Security("has_role('ROLE_ADMIN', 'ROLE_USER')")
 */
class DebtController extends Controller
{
    /**
     * @Route("/debt/form/{id}")
     * @Route("/debt/form/{id}/{type}")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function formAction(Subject $subject, $type = null)
    {
        $debt = new Debt();
        $debt->setSubject($subject);

        if ($type !== null) {
            $debt->setType($type);
        }

        $form = $this->createForm(new DebtType(), $debt, [
            "action" => $this->generateUrl("debt_save_form")
        ]);

        return $this->render("@App/Debt/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("debt/save", name="debt_save_form")
     * @Route("debt/update/{id}", name="debt_update_form")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveForm(Request $request, Debt $debt = null)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getEntityManager();
        $flagNew = false;

        $id = $request->get("appbundle_debt")["subjectId"];

        $sbj = $em->getRepository("AppBundle:Subject")->find($id);


        if ($debt === null) {
            $flagNew = true;
            $debt = new Debt();
        }
        $debt->setSubject($sbj);

        $form = $this->createForm(new DebtType(), $debt);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($flagNew) {
                $type = $form->get("type")->getData();
                $issetDebt = $em->getRepository("AppBundle:Debt")->findBy([
                    "subject" => $sbj,
                    "type" => $type
                ]);

                if (is_null($issetDebt) && Debt::$valueTypes[$type] == Debt::SINGLE) {
                    throw new BadRequestHttpException("Такой таск уже существует");
                }

                if (is_null($issetDebt) && Debt::$valueTypes[$type] == Debt::BUNDLE) {
                    throw new BadRequestHttpException("Такой таск уже существует");
                }

                //TODO: Добавить групировку к существующим
                $count = $form->get("count")->getData();
                $em->persist($form->getData());
                $em->flush();
                $this->get("app.component.debt_child_manager")->createChilds($debt, $count);
            }
            return new FormAcceptedResponse();
        }

        return $this->render("@App/Debt/form.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/debt/list/{id}")
     * @Route("/debt/list/{id}/{hide}")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function testFormAction(Subject $subject, $hide = null)
    {

        if ($hide) {
            if ($hide == "hide") {
                $val = $this->getDoctrine()->getRepository("AppBundle:Debt")->getNotClosed($subject);
            } elseif ($hide == "closed") {
                $val = $this->getDoctrine()->getRepository("AppBundle:Debt")->getClosed($subject);
            }
        } else {
            $val = $this->getDoctrine()->getRepository("AppBundle:Debt")->findBy(["subject" => $subject]);
        }

        return $this->render("@App/Debt/list.html.twig", [
            "debtList" => $val
        ]);
    }

    /**
     *
     * @Route("/debt/save/{id}")
     * @param Request $request
     * @param DebtValue $value
     * @return FormAcceptedResponse
     */
    public function debtValueSaveAction(Request $request, DebtValue $value)
    {
        $val = $request->get("value");

        if (is_null($val)) {
            throw  new BadRequestHttpException("incorrect value");
        }

        $value->setValue($val);
        $this->getDoctrine()->getManager()->flush();

        return new FormAcceptedResponse();
    }

    /**
     * @return Response
     * @Route("/statistics/debt/week")
     */
    public function getStatisticsByWeek()
    {
        $date = new \DateTime();
        $date = new CalendarDate($date->format("Y-m-d"));

        $totalResult = $this->getStatisticsByDateRage($date->getWeekStart(), $date->getWeekEnd());

        return new JsonResponse($totalResult);
    }

    /**
     * @return Response
     * @Route("/statistics/debt/month")
     */
    public function getStatisticsByMonth()
    {
        $date = new \DateTime();
        $dateA = new \DateTime($date->format("Y-m-1"));
        $dateB = new \DateTime($date->format("Y-m-t 23:59:59"));

        $totalResult = $this->getStatisticsByDateRage($dateA, $dateB);

        return new JsonResponse($totalResult);
    }

    private function getStatisticsByDateRage(\DateTime $dateA, \DateTime $dateB)
    {
        $values = $this->getDoctrine()->getRepository("AppBundle:DebtValue")->getByDateRangeAndUser($this->getUser(),
            $dateA, $dateB);

        $result = CalendarDate::getArrayWithDateKeys($dateA, $dateB);
        foreach ($result as &$val) {
            $val = [
                "value" => 0,
                "lab" => 0,
                "practice" => 0,
                "other" => 0,
            ];
        }

        /** @var DebtValue $value */
        foreach ($values as $value) {
            if (array_key_exists($value->getDateModified()->format("Y-m-d"), $result)) {
//                $result[$value->getDateModified()->format("Y-m-d")]["value"] += 1;
                switch ($value->getDebt()->getType()) {
                    case Debt::TYPE_LAB:
                        $result[$value->getDateModified()->format("Y-m-d")]["lab"] += 1;
                        break;
                    case Debt::TYPE_PRACT_RAB:
                        $result[$value->getDateModified()->format("Y-m-d")]["practice"] += 1;
                        break;
                    default:
                        $result[$value->getDateModified()->format("Y-m-d")]["other"] += 1;
                        break;
                }
            }
        }
        $totalResult = [];
        foreach ($result as $key => $val) {
            $totalResult[] = [
                "date"      => $key,
                "lab"       => $val["lab"],
                "practice"  => $val["practice"],
                "other"     => $val["other"]
            ];
        }

        return $totalResult;
    }

}
